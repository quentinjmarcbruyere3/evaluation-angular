import { Injectable } from '@angular/core';
import { Statistique } from './models/statistique';
import { HttpClient } from '@angular/common/http';
import { StatistiqueBack } from './models/apiType';
import { webSocket } from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root'
})
export class StatistiqueService {

  statistiques: Statistique[] = [];

  constructor(
    private http: HttpClient
  ) { 
	  this.connectToWs();
  }

  getStatistique(){
	this.statistiques = [];
    this.http.get<StatistiqueBack[]>("https://stats.naminilamy.fr").subscribe(
      res => {
        for (const statistique of res) {
          this.statistiques.push(new Statistique(statistique.id, statistique.title, statistique.value, statistique.updatedAt));
        }
      });
  }

  envoieStatistique(statistique: Statistique){
	this.http.post<Statistique>("https://stats.naminilamy.fr", {title: statistique.title, value: statistique.value}).subscribe(
		() => {
			this.getStatistique();
		}
	);
  }

  supprimerStatistique(statistique: Statistique){
	  this.http.delete<Statistique>("https://stats.naminilamy.fr?id=" + statistique.id).subscribe(
		  res => {
			  console.log(res);
			  this.getStatistique();
		  }
	  )
  }

  connectToWs(){
	  webSocket("wss://ac88n1oa17.execute-api.eu-west-3.amazonaws.com/dev").subscribe(
		  (msg: any) => {
			this.statistiques.push(new Statistique(msg.object.id, msg.object.title, msg.object.value, msg.object.updatedAt));
		  },
		  err => console.log(err),
		  () => {
			  console.log("Websocket disconnect");
			  this.connectToWs();
		  }
	  )
  }
}
