import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormulaireStatistiqueComponent } from './formulaire-statistique.component';

describe('FormulaireStatistiqueComponent', () => {
  let component: FormulaireStatistiqueComponent;
  let fixture: ComponentFixture<FormulaireStatistiqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormulaireStatistiqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormulaireStatistiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
