import { Component, OnInit } from '@angular/core';
import { Statistique } from '../models/statistique';
import { StatistiqueService } from '../statistique.service';
import { UUID } from 'angular2-uuid';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulaire-statistique',
  templateUrl: './formulaire-statistique.component.html',
  styleUrls: ['./formulaire-statistique.component.css']
})
export class FormulaireStatistiqueComponent implements OnInit {

  title: string = "";
  value: string = "";

  constructor(
    public statistiqueService: StatistiqueService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  ajouterStatistique(){
    const current = new Date();
	this.statistiqueService.envoieStatistique(new Statistique(UUID.UUID(), this.title, this.value, current.getTime().toString()));
    this.router.navigate(["/liste"]);
  }

}
