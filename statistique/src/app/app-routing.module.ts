import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AffichageStatistiqueComponent } from './affichage-statistique/affichage-statistique.component';
import { FormulaireStatistiqueComponent } from './formulaire-statistique/formulaire-statistique.component';

const routes: Routes = [
  {path: "liste", component: AffichageStatistiqueComponent},
  {path: "nouveau", component: FormulaireStatistiqueComponent},
  {path: "**", component: AffichageStatistiqueComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
